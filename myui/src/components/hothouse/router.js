// import P from "@/page/P";
//
// let houseList = resolve => require(['@/page/hothouse/houseList'], resolve);
// let addnewhouse = resolve => require(['@/page/hothouse/addnewhouse'], resolve);
// let housePricelist = resolve => require(['@/page/hothouse/housePricelist'],resolve)
// let houseStyle = resolve => require(['@/page/hothouse/houseStyle'],resolve)
// let houseType = resolve => require(['@/page/hothouse/houseType'],resolve)
// let AddType = resolve => require(['@/page/hothouse/AddType'],resolve)
// let AddStyle = resolve => require(['@/page/hothouse/AddStyle'],resolve)
// let AddHousePrice = resolve => require(['@/page/hothouse/AddHousePrice'],resolve)
// let Houseupdate = resolve => require(['@/page/hothouse/Houseupdate'],resolve)
// export default {
//   path: "hot/house",
//   component: P,
//   children: [
//     {
//       path: 'list',
//       name: 'houseList',
//       component: houseList
//     },
//     {
//       path:'add',
//       name:'addnewhouse',
//       component:addnewhouse
//     },
//     {
//       path:'update/:id',
//       name:'Houseupdate',
//       component:Houseupdate
//     },
//     {
//       path:'price/list/:id',
//       name:'housePricelist',
//       component:housePricelist
//     },
//     {
//       path:'price/add/:id',
//       name:'AddHousePrice',
//       component:AddHousePrice
//     },
//     {
//       path:'style/list',
//       name:'houseStyle',
//       component:houseStyle
//     },
//     {
//       path:'type/list',
//       name:'houseType',
//       component:houseType
//     },
//     {
//       path:'type/add',
//       name:'AddType',
//       component:AddType
//     },
//     {
//       path:'style/add',
//       name:'AddStyle',
//       component:AddStyle
//     }
//   ]
// }
