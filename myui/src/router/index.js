import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from "@/Login";
import Home from "@/views/Home";
import Student_Table from "@/components/Rent/Rent_Table";
import Admin_Table from "@/components/Tenant/Tenant_Table";
import YAdminMessage from "@/components/YAdmin/YAdminMessage";
import YCharts from "@/components/YAdmin/YCharts";


import bannerList from '@/components/hothouse/RoomList'

import ActivityUpdate from "@/components/activity/ActivityUpdate";
import activitylist from "@/components/activity/activitylist";
import addactivity from "@/components/activity/addactivity";
import Houseupdate from "@/components/hothouse/RoomUpdate";
import addnewhouse from "@/components/hothouse/addnewhouse";
import housePricelist from "@/components/hothouse/housePricelist";
import AddHousePrice from "@/components/hothouse/AddHousePrice";
import houseStyle from "@/components/hothouse/houseStyle";
import houseType from "@/components/hothouse/houseType";
import AddType from "@/components/hothouse/AddType";
import AddStyle from "@/components/hothouse/AddStyle";
import lease from "@/components/Tenant/lease";
import Goods from "@/components/hothouse/Goods";
import Lent_Chart from "@/components/Rent/Rent_Chart";
import TestMain from "@/components/test/TestMain";
import Chart1 from "@/components/YChat/Chart_Turnover";
import Chart2 from "@/components/YChat/Chart_Trivelflow";
import YuYueView from "@/components/Yuyue/YuYueView";
import MyMessage from "@/components/Yuyue/MyMessage";
import Chat_Active from "@/Chat_Active";
import ChangePassword from "@/components/YAdmin/ChangePassword";
import T from "@/components/T";


Vue.use(VueRouter)


const routes = [
  {
    path: '/',
    // name: 'Chat_Active',
    // component: Chart2
    // path: '/',
    component:Login

  },
  {
    path: "/YuYue",
    component: YuYueView,
    children: [{
      path: "/MyMessage",
      component: MyMessage
    }]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },{
  path: '/home',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */'../components/YAdmin/YHome'),
    children: [
      {
        path:'/Students',
        component:Student_Table
      },
      {
        path:'/Admin',
        component:Admin_Table
      },{
        path:'/YAdminMessage',
        component:YAdminMessage
      },
      // {
      //   path:'/StudentCompute',
      //   component:StudentCompute
      // }
      {
        path: '/list',
        name: 'activitylist',
        component: activitylist
      },
      {
        path:'/add',
        name:'addactivity',
        component:addactivity
      },
      {
        path:'/update/:id',
        name:'ActivityUpdate',
        component:ActivityUpdate
      },
        //
      {
        path: '/houseList',
        name: 'houseList',
        component: bannerList
      },
      {
        path:'/addnewhouse',
        name:'addnewhouse',
        component:addnewhouse
      },
      {
        path:'/Houseupdate',
        name:'Houseupdate',
        component:Houseupdate
      },
      {
        path:'/housePricelist',
        name:'housePricelist',
        component:housePricelist
      },
      {
        path:'/AddHousePrice',
        name:'AddHousePrice',
        component:AddHousePrice
      },
      {
        path:'/houseStyle',
        name:'houseStyle',
        component:houseStyle
      },
      {
        path:'/houseType',
        name:'houseType',
        component:houseType
      },
      {
        path:'/AddType',
        name:'AddType',
        component:AddType
      },
      {
        path:'/AddStyle',
        name:'AddStyle',
        component:AddStyle
      },
      {
        path: '/lease',
        component: lease
      },{
    path:'/Goods',
        component: Goods
      },{
    path:'/Lent_Chart',
        component: Lent_Chart
      },{
    path: "/Chat1",
        component: Chart1
      },{
    path: "/Chart2",
        component: T
      },{
    path: "/Chat_Active",
        component: Chat_Active
      },{
    path: "/ChangePassword",
        component: ChangePassword
      }
    ]

  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
