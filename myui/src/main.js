import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'
import axios from "axios";
import router from './router'
import Bus from './util/bus'


Vue.use(ElementUI);
import * as echarts from 'echarts'
Vue.prototype.$axios=axios;
// axios.defaults.baseURL="http://localhost:8080"
axios.defaults.baseURL="/api"
Vue.prototype.$echarts = echarts
Vue.prototype.$bus = Bus;
Vue.config.productionTip = false
Vue.config.silent = true
Vue.config.productionTip = false


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
