module.exports = {
  // 选项...
  lintOnSave: false,
  outputDir:'../src/main/webapp',//更改BUILD输出的目录
  configureWebpack: {
    //关闭 webpack 的性能提示
    performance: {
      hints:false
    }

  },
  devServer: {
    port: 8085,
    open: true,// vue项目启动时自动打开浏览器
    proxy: {
      '/api': { // '/api'是代理标识，用于告诉node，url前面是/api的就是使用代理的
        target: "http://localhost:8080/", //目标地址，一般是指后台服务器地址
        changeOrigin: true, //是否跨域
        pathRewrite: { // pathRewrite 的作用是把实际Request Url中的'/api'用""代替
          '^/api': ""
        }
      }
    }
  }

  // proxy: {  //配置跨域
  //   '/api': {
  //     target: 'http://localhost:8080/',  //这里后台的地址模拟的;应该填写你们真实的后台接口
  //     changOrigin: true,  //允许跨域
  //     pathRewrite: {
  //       /* 重写路径，当我们在浏览器中看到请求的地址为：http://localhost:8080/api/core/getData/userInfo 时
  //         实际上访问的地址是：http://121.121.67.254:8185/core/getData/userInfo,因为重写了 /api
  //        */
  //       '^/api': ''
  //     }
  //   },
  // }
}
