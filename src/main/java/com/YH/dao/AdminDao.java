package com.YH.dao;

import com.YH.entity.Admin;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.awt.print.Pageable;
import java.util.List;

/**
 * (Admin)表数据库访问层
 *
 * @author YH
 * @since 2021-12-20 10:59:42
 */
@Component
public interface AdminDao {
        //登陆操作
        boolean IsAdmin(Admin A);
        //注册
        void Register(Admin A);
        //修改
        void Update(Admin A);
         List GetUsers();
        //删除用户
        void DeleteUser(Admin A);
         //更新用户信息
}

