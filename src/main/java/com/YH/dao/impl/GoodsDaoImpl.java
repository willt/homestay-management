package com.YH.dao.impl;

import com.YH.dao.GoodsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class GoodsDaoImpl implements GoodsDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public List GetAllGoodList() {

        String str="select * from myporject.goods";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(str);
        return maps;
    }
}
