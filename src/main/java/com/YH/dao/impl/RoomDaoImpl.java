package com.YH.dao.impl;

import com.YH.dao.RoomDao;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class RoomDaoImpl implements RoomDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    Date D;
    @Override
    public List GetAllRoom() {
        List<Map<String, Object>> maps = new ArrayList<>();
        String str="select * from myporject.room";
        maps = jdbcTemplate.queryForList(str);
         return maps;
    }
}
