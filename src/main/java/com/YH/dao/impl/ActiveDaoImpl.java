package com.YH.dao.impl;

import com.YH.dao.ActiveDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ActiveDaoImpl implements ActiveDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public List GetAllActive() {
        String  str = "select * FROM myporject.active";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(str);
        return maps;
    }
}
