package com.YH.dao.impl;

import com.YH.dao.TenantDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class TenantDaoimpl implements TenantDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public List GetAllTenants() {
        String str = "select * from myporject.tenant";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(str);

        return maps;
    }
}
