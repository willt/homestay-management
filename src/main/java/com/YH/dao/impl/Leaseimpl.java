package com.YH.dao.impl;

import com.YH.dao.LeaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class Leaseimpl implements LeaseDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public List GetAllLeases() {
        String str = "select * from myporject.lease";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(str);
        return maps;
    }
}
