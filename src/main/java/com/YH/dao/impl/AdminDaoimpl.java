package com.YH.dao.impl;

import com.YH.dao.AdminDao;
import com.YH.entity.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class AdminDaoimpl implements AdminDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public boolean IsAdmin(Admin A) {
        boolean flag = false;
        String str="select account,password from myporject.admin Where account = ? and password = ?";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(str,A.getAccount(),A.getPassword());
        if(maps.size()==0){
            System.out.println("No");
        }else{
            flag = true;
            System.out.println("Not Null");
            System.out.println(maps.size());
        }
        return flag;
    }

    @Override
    public void Register(Admin A) {
        String str = "INSERT myporject.admin(account, password) VALUES (?,?)";
        if(!IsAdmin(A)){
        jdbcTemplate.update(str,A.getAccount(),A.getPassword());
        }
    }

    @Override
    public void Update(Admin A) {
        String str="update myporject.admin SET password=? where account=?";
        jdbcTemplate.update(str,A.getPassword(),A.getAccount());
    }

    @Override
    public List GetUsers() {
        String str = "select * FROM myporject.admin ";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(str);
        return maps;
    }

    @Override
    public void DeleteUser(Admin A) {
        String str = "delete from myporject.admin where account=?";
            jdbcTemplate.update(str,A.getAccount());
    }
}
