package com.YH.myproject;

import com.YH.dao.impl.AdminDaoimpl;
import com.YH.entity.Admin;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

/*
在这里初始化并获取Spring的上下文
 */
@WebListener
public class ServletContextListener implements javax.servlet.ServletContextListener {
    public static ApplicationContext context;
    @Override
    public void contextInitialized(ServletContextEvent sce) {
//        WebApplicationContext springContext= WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());//获取初始化的上下文
//        AdminDaoimpl bean = springContext.getBean(AdminDaoimpl.class);

        System.out.println("初始化ServletContext");
//         sce.getServletContext().setAttribute("bean",bean);
         context = new ClassPathXmlApplicationContext("SpringContext.xml");
//        AdminDaoimpl bean = context.getBean(AdminDaoimpl.class);
        sce.getServletContext().setAttribute("bean",context);


//        System.out.println(bean.GetUsers());;

//        Admin A = new Admin();
//        A.setAccount("admin1");
//        A.setPassword("123456");
//        bean.IsAdmin(A);
//        AdminDaoimpl bean = context.getBean(AdminDaoimpl.class);
    }
}
