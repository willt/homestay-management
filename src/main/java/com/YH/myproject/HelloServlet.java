package com.YH.myproject;

import com.YH.dao.impl.AdminDaoimpl;
import com.YH.entity.Admin;
import com.YH.entity.MyData;
import com.google.gson.Gson;
import org.springframework.context.ApplicationContext;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "helloServlet", value = "/test")
public class HelloServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        String data = "xx:xx";



        PrintWriter out = response.getWriter();
        Admin A = new  Admin();
        A.setAccount("111");
        A.setPassword("1111");
        Gson gson = new Gson();
        ApplicationContext context = (ApplicationContext) request.getServletContext().getAttribute("bean");
        AdminDaoimpl bean = context.getBean(AdminDaoimpl.class);
//        AdminDaoimpl bean = (AdminDaoimpl) getServletContext().getAttribute("bean");

//        System.out.println(gson.toJson(bean.GetUsers()));
//        System.out.println(gson.toJson(new MyData(bean.GetUsers())));
        out.println( gson.toJson(new MyData(bean.GetUsers())));
//        out.println( gson.toJson(A));
    }
    public void destroy() {
    }
}
