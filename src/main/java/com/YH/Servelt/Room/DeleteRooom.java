package com.YH.Servelt.Room;

import com.YH.entity.Room;
import com.YH.service.impl.RoomServiceImpl;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@WebServlet("/DeleteRooom")
public class DeleteRooom extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        /*
        {hotel_name:"sadas",in_time:'sss',room_number:"22222",room_price:48,room_type:"",lent_state:""}
         */
        Room R = new Room();
        R.setHotelName(req.getParameter("hotel_name"));
        R.setRoomNumber(req.getParameter("room_number"));
        try {
            R.setInTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(req.getParameter("in_time")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        R.setLentState(req.getParameter("lent_state"));
        R.setPrice(req.getParameter("room_price"));
        R.setRoomType(req.getParameter("room_type"));
        RoomServiceImpl bean = context.getBean(RoomServiceImpl.class);
        bean.DeleteRoom(R);
    }
}
