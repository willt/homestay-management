package com.YH.Servelt.Goods;

import com.YH.entity.Goods;
import com.YH.service.impl.GoodsServiceImpl;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/DeleteGood")
public class DeleteGood extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Goods G = new Goods();
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        G.setGoodCount(Integer.valueOf(req.getParameter("good_count")));
        G.setGoodName(req.getParameter("good_name"));
        G.setGoodNumber(req.getParameter("good_count"));
        G.setGoodValue(Integer.valueOf(req.getParameter("good_value")));
        G.setHotelName(req.getParameter("hotel_name"));
        G.setRoomNumber(req.getParameter("room_number"));
        GoodsServiceImpl bean = context.getBean(GoodsServiceImpl.class);
        bean.DeleteGood(G);
    }
}
