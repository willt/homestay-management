package com.YH.Servelt.Charts;

import com.YH.entity.ChartData;
import com.YH.service.impl.MyServicelmpl;
import com.google.gson.Gson;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet("/GetYearsCount")
public class GetYearsCount extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        MyServicelmpl bean = context.getBean(MyServicelmpl.class);
        List list = bean.CountMoneyByTime(2021);
        List Keys = new ArrayList();
        List Valus = new ArrayList();

        for (Object i:list){
            Map<String, Object> A = (Map<String, Object>) i;
            Keys.add(A.get("民宿名称"));
            Valus.add(A.get("年度营业额"));
        }
        ChartData D = new ChartData();
        D.setKeys(Keys);
        D.setValues(Valus);

        resp.setContentType("text/html;charset=utf-8");
        PrintWriter writer = resp.getWriter();
        writer.println(new Gson().toJson(D));
    }
}
