package com.YH.Servelt.Charts;

import com.YH.entity.ChartData;
import com.YH.entity.TheTwoCharData;
import com.YH.service.impl.MyServicelmpl;
import com.google.gson.Gson;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@WebServlet("/CountByMonthNum")
public class CountByMonthNum extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        MyServicelmpl bean = context.getBean(MyServicelmpl.class);
        List list = bean.CountByMonthNum();
        List hotel_name = new ArrayList();
        List Month = new ArrayList();
        List Count = new ArrayList();
        for (Object i:list){
            Map<String, Object> A = (Map<String, Object>) i;
            hotel_name.add(A.get("HotelName"));
            Month.add(A.get("Month"));
            Count.add(A.get("Count"));
        }
        hotel_name.forEach(o->{
            System.out.println(o);
        });
        TheTwoCharData D = new TheTwoCharData();
        D.setHotel_name(hotel_name);
        D.setCount(Count);
        D.setMonth(Month);
        System.out.println(new Gson().toJson(D));
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter writer = resp.getWriter();
        writer.println(new Gson().toJson(D));
    }
}
