package com.YH.Servelt.Active;

import com.YH.dao.impl.ActiveDaoImpl;
import com.google.gson.Gson;
import org.springframework.context.ApplicationContext;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "GetAllActive", value = "/GetAllActive")
public class GetAllActive extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        PrintWriter writer = response.getWriter();
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        ActiveDaoImpl bean = context.getBean(ActiveDaoImpl.class);
        writer.println(new Gson().toJson( bean.GetAllActive()));
    }


}
