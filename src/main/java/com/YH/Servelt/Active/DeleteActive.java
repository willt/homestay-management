package com.YH.Servelt.Active;

import com.YH.entity.Active;
import com.YH.service.impl.ActiveServiceImpl;
import org.springframework.context.ApplicationContext;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@WebServlet(name = "DeleteActive", value = "/DeleteActive")
public class DeleteActive extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Active A =new Active();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        ft.setLenient(false);
        try {

            A.setActiveStarttime(ft.parse(request.getParameter("active_starttime")==null ? "0000-00-00":request.getParameter("active_starttime") ));
            A.setActiveEndtime(ft.parse(request.getParameter("active_endtime")));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        A.setActiveHotel(request.getParameter("active_hotel"));
//        A.setActiveEndtime(Date.valueOf(request.getParameter("active_endtime")));
        A.setActiveName(request.getParameter("active_name"));
        A.setActiveNumber(request.getParameter("active_number"));
//        A.setActiveStarttime(Date.valueOf(request.getParameter("active_starttime")));
        A.setRemark(request.getParameter("Remark"));
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        ActiveServiceImpl bean = context.getBean(ActiveServiceImpl.class);
        bean.DeleteService(A);
    }
}
