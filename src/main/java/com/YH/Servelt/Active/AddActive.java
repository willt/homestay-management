package com.YH.Servelt.Active;

import com.YH.entity.Active;
import com.YH.service.impl.ActiveServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.format.annotation.DateTimeFormat;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;

@WebServlet(name = "AddActive", value = "/AddActive")
public class AddActive extends HttpServlet {
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Active A =new Active();
        A.setActiveHotel(request.getParameter("active_hotel"));
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        ft.setLenient(false);
        String active_starttime = request.getParameter("active_starttime");
        String active_endtime = request.getParameter("active_endtime");
        System.out.println(active_starttime);
        System.out.println(active_endtime);
        try {

                A.setActiveStarttime(ft.parse(active_starttime));
                A.setActiveEndtime(ft.parse(active_endtime));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        A.setActiveName(request.getParameter("active_name"));
        A.setActiveNumber(request.getParameter("active_number"));
        A.setRemark(request.getParameter("Remark"));
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        ActiveServiceImpl bean = context.getBean(ActiveServiceImpl.class);
        bean.AddService(A);
    }

}
