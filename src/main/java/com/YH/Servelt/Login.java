package com.YH.Servelt;

import com.YH.dao.impl.AdminDaoimpl;
import com.YH.entity.Admin;
import com.YH.entity.MyData;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class Login extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        MyData data;
        Admin A = new Admin();
        PrintWriter writer = resp.getWriter();
        A.setAccount( req.getParameter("account"));
        A.setPassword(req.getParameter("password"));
        A.getAccount();
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        AdminDaoimpl bean = context.getBean(AdminDaoimpl.class);
        if(bean.IsAdmin(A)){
            //账号密码正确
            data = new MyData("200");
        }else{
            data = new MyData("300");
        }
        writer.write(data.GetDatas());
    }
}
