package com.YH.Servelt.Leace;

import com.YH.entity.Lease;
import com.YH.service.impl.LeaceSeriveImpl;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AddLeaser")
public class AddLeaser extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Lease L = new Lease();
        /*
        hotel_name: '2016-05-02',
        reserver_name: '王小虎',
        room_number: '上海',
        reseve_days: '普陀区',
        reseve_price: '上海市普陀区金沙江路 1518 弄',
         */
        L.setHotelName(req.getParameter("hotel_name"));
        L.setRoomNumber(req.getParameter("room_number"));
        L.setReserverName(req.getParameter("reserver_name"));
        L.setReseveDays(Integer.valueOf(req.getParameter("reseve_days")));
        L.setResevePrice(req.getParameter("reseve_price"));
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        LeaceSeriveImpl bean = context.getBean(LeaceSeriveImpl.class);
        bean.AddLeaser(L);
    }
}
