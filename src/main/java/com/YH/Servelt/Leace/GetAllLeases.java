package com.YH.Servelt.Leace;

import com.YH.dao.impl.Leaseimpl;
import com.YH.dao.impl.RendDaoimpl;
import com.google.gson.Gson;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/GetAllLeases")
public class GetAllLeases extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter writer = resp.getWriter();
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        Leaseimpl bean = context.getBean(Leaseimpl.class);
        writer.println(new Gson().toJson( bean.GetAllLeases()));
    }
}
