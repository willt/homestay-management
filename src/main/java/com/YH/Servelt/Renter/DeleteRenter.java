package com.YH.Servelt.Renter;

import com.YH.entity.Rent;
import com.YH.service.impl.RendServiceImpl;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/DeleteRenter")
public class DeleteRenter extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Rent R = new Rent();
        PrintWriter writer = resp.getWriter();
        R.setAge(req.getParameter("age"));
        R.setHotelName(req.getParameter("hotel_name"));
        R.setName(req.getParameter("name"));
        R.setIdNumber(req.getParameter("id_number"));
        R.setPhone(req.getParameter("phone"));
        R.setSex(req.getParameter("sex"));
        R.setRoomNumber(req.getParameter("room_number"));
        R.setRecerverDays(Integer.valueOf(req.getParameter("recerver_days")));
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        RendServiceImpl bean = context.getBean(RendServiceImpl.class);
        bean.DeleteRenter(R);
    }
}
