package com.YH.Servelt.Renter;

import com.YH.dao.impl.RendDaoimpl;
import com.google.gson.Gson;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/GetAllRenter")
public class GetAllRenter extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       resp.setContentType("text/html;charset=utf-8");
        PrintWriter writer = resp.getWriter();
        ApplicationContext context = (ApplicationContext) getServletContext().getAttribute("bean");
        RendDaoimpl bean = context.getBean(RendDaoimpl.class);
        writer.println(new Gson().toJson( bean.GetAllLendList()));
    }
}
