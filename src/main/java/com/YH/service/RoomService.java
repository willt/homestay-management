package com.YH.service;


import com.YH.entity.Room;

import java.util.List;

public interface RoomService {
    List AddRoom(Room R);
    List UpdateRoom(Room R);
    List DeleteRoom(Room R);
}
