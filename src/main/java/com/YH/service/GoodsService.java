package com.YH.service;

import com.YH.entity.Goods;

import java.util.List;

public interface GoodsService {
    List AddGood(Goods G);
    List UpdateGood(Goods G);
    List DeleteGood(Goods G);
}
