package com.YH.service;

import com.YH.entity.Rent;

import java.util.List;

public interface MyService {
    /*
    1.查询所有民宿租房数(CountAllHotelNum)
    2.按照年查询所有民宿的营收额(CountMoneyByTime)
    3.按照地址，租金金范围，房间类型查询满足条件的民宿(SerchHotel)
    4.查询某个民宿可用房间情况(SerchHotelRoom)
    5.按照旅馆名字查询旅馆负责人的姓名,电话号码和地址(SerchMasterByHName)
    6.可以根据满足指定类型的租客，按照活动进行打折计算出最后的结算金额（Discount）
    7.获取每个民宿的预约人数（GetLenter）
    8.按照月份的方式，获取每个民宿的租房数量(CountByMonthNum)
    9.根据指定的民宿名称，如果该民宿有活动，则返回民宿活动的名称、开始时间和结束时间(GetActiveTimeByName)
    10.根据指定的民宿名称，指定的年份，获取该民宿每个月份的总预约数(GetRentByNameAndMonth)
     */
    List CountAllHotelNum();
    List CountMoneyByTime(int Year);
//    List SerchHotel();
    List SerchHotelRoom();
//    List SerchMasterByHName();
    void MuDiscount(String hotel_name,String Leaser_name,String Lease_Type,Double count);
    List GetLenter();
    List CountByMonthNum();
    List GetActiveTimeByName(String hotel_name);
    List GetRentByNameAndMonth(String hotel_name,int Month);
    void AddRentSericve(Rent R);

}
