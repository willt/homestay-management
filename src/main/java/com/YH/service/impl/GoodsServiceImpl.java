package com.YH.service.impl;

import com.YH.entity.Goods;
import com.YH.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public List AddGood(Goods G) {
        String str = "INSERT INTO myporject.goods VALUES (?,?,?,?,?,?)";
        jdbcTemplate.update(str,G.getHotelName(),G.getRoomNumber(),G.getGoodNumber(),G.getGoodName(),G.getGoodValue(),G.getGoodCount());
        return null;
    }

    @Override
    public List UpdateGood(Goods G) {
        String str="update myporject.goods SET hotel_name=?,room_number=?,good_number=?,good_name=?,good_value=?,good_count=? WHERE hotel_name=? AND room_number=? AND  good_number=?";
        jdbcTemplate.update(str,G.getHotelName(),G.getRoomNumber(),G.getGoodNumber(),G.getGoodName(),G.getGoodValue(),G.getGoodCount(),G.getHotelName(),G.getRoomNumber(),G.getGoodNumber());
        return null;
    }

    @Override
    public List DeleteGood(Goods G) {
        String str="delete  FROM  myporject.goods WHERE hotel_name=? AND room_number=? AND  good_number=?";
        jdbcTemplate.update(str,G.getHotelName(),G.getRoomNumber(),G.getGoodNumber());
        return null;
    }
}
