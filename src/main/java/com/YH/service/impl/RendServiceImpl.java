package com.YH.service.impl;

import com.YH.service.RendService;
import com.YH.entity.Rent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RendServiceImpl implements RendService {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List AddRenter(Rent R) {
        String str = "INSERT INTO myporject.rent VALUES (?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(str,R.getName(),R.getRoomNumber(),R.getRecerverDays(),R.getHotelName(),R.getAge(),R.getPhone(),R.getIdNumber(),R.getSex());
        return null;
    }

    @Override
    public List UpdateRenter(Rent R) {
        String str="update myporject.rent SET name=?,room_number=?,recerver_days=?,hotel_name=?,age=?,phone=?,Id_number=? ,sex=? WHERE name=?";
        jdbcTemplate.update(str,R.getName(),R.getRoomNumber(),R.getRecerverDays(),R.getHotelName(),R.getAge(),R.getPhone(),R.getIdNumber(),R.getSex(),R.getName());
        return null;
    }

    @Override
    public List DeleteRenter(Rent R) {
        String str="delete  FROM  myporject.rent WHERE name=?";
        jdbcTemplate.update(str,R.getName());

        return null;
    }
}
