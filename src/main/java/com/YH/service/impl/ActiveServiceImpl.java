package com.YH.service.impl;

import com.YH.entity.Active;
import com.YH.service.ActiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class ActiveServiceImpl implements ActiveService {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Override
    public void AddService(Active A) {
        String str = "insert into myporject.active VALUES(?,?,?,?,?,?)";
        jdbcTemplate.update(str,A.getActiveNumber(),A.getActiveName(),A.getActiveHotel(),A.getActiveStarttime(),A.getActiveEndtime(),A.getRemark());

    }

    @Override
    public void UpdateService(Active A) {
    String str = "update myporject.active set active_number=?,active_name=?,active_hotel=?,active_starttime=?,active_endtime=?,Remark=? WHERE active_number=?";
    jdbcTemplate.update(str,A.getActiveNumber(),A.getActiveName(),A.getActiveHotel(),A.getActiveStarttime(),A.getActiveEndtime(),A.getRemark(),A.getActiveNumber());

    }

    @Override
    public void DeleteService(Active A) {
    String str = "delete FROM myporject.active where active_number=?";
    jdbcTemplate.update(str,A.getActiveNumber());

    }
}
