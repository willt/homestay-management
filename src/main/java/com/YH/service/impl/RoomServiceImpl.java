package com.YH.service.impl;

import com.YH.entity.Room;
import com.YH.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class RoomServiceImpl implements RoomService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List AddRoom(Room R) {

        String str="insert into  myporject.room  VALUES (?,?,?,?,?,?)";
        jdbcTemplate.update(str,R.getRoomNumber(),R.getRoomType(),R.getPrice(),R.getHotelName(),R.getInTime(),R.getLentState());
        return null;
    }

    @Override
    public List UpdateRoom(Room R) {
        String str=" UPDATE myporject.room set room_number=?,room_type=?,room_price=?,hotel_name=?,in_time=?,lent_state=? where room_number=?";
        jdbcTemplate.update(str,R.getRoomNumber(),R.getRoomType(),R.getPrice(),R.getHotelName(),R.getInTime(),R.getLentState(),R.getRoomNumber());

        return null;
    }

    @Override
    public List DeleteRoom(Room R) {
        String str="delete FROM myporject.room WHERE room_number=?";
        jdbcTemplate.update(str,R.getRoomNumber());

        return null;
    }
}
