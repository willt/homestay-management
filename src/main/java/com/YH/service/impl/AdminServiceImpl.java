package com.YH.service.impl;

import com.YH.dao.AdminDao;
import com.YH.entity.Admin;
import com.YH.service.AdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (Admin)表服务实现类
 *
 * @author YH
 * @since 2021-12-20 10:59:44
 */
@Service("adminService")
public class AdminServiceImpl implements AdminService {
    @Resource
    private AdminDao adminDao;

    @Override
    public Admin queryById(Integer id) {
        return null;
    }

    @Override
    public Admin insert(Admin admin) {
        return null;
    }

    @Override
    public Admin update(Admin admin) {
        return null;
    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
//    @Override
//    public Admin queryById(Integer id) {
//        return this.adminDao.queryById(id);
//    }
//
//    /**
//     * 分页查询
//     *
//     * @param admin 筛选条件
//     * @param pageRequest      分页对象
//     * @return 查询结果
//     */
//
//
//    /**
//     * 新增数据
//     *
//     * @param admin 实例对象
//     * @return 实例对象
//     */
//    @Override
//    public Admin insert(Admin admin) {
//        this.adminDao.insert(admin);
//        return admin;
//    }
//
//    /**
//     * 修改数据
//     *
//     * @param admin 实例对象
//     * @return 实例对象
//     */
//    @Override
//    public Admin update(Admin admin) {
//        this.adminDao.update(admin);
//        return this.queryById(admin.getId());
//    }
//
//    /**
//     * 通过主键删除数据
//     *
//     * @param id 主键
//     * @return 是否成功
//     */
//    @Override
//    public boolean deleteById(Integer id) {
//        return this.adminDao.deleteById(id) > 0;
//    }
}
