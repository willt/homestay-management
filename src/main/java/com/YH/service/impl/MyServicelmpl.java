package com.YH.service.impl;

import com.YH.entity.Rent;
import com.YH.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MyServicelmpl implements MyService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List CountAllHotelNum() {
        List<Map> ReList = new ArrayList<>();
        String str = " { call myporject.CountAllHotelNum()}";
        jdbcTemplate.execute(connection -> {
            CallableStatement cs = connection.prepareCall(str);
            return cs;
        },(CallableStatementCallback<List>) callableStatement -> {
            callableStatement.execute();
            ResultSet Rs = callableStatement.getResultSet();
//                ResultSet Rs = (ResultSet) callableStatement.getObject(1);
            while (Rs.next()) {
                Map Row = new HashMap();
                Row.put("民宿名", Rs.getString("hotel_name"));
                Row.put("租房数", Rs.getString("租房数"));
                ReList.add(Row);
            }
            return ReList;
        } );
        return ReList;
    }

    @Override
    public List CountMoneyByTime(int Year) {
        List<Map> ReList = new ArrayList<>();
        String str = " { call myporject.CountMoneyByTime(?)}";
        jdbcTemplate.execute(new CallableStatementCreator() {
            List ResList = new ArrayList();
            @Override
            public CallableStatement createCallableStatement(Connection connection) throws SQLException {
                CallableStatement Cs = connection.prepareCall(str);
                Cs.setInt(1,Year);
                return Cs;
            }
        }, callableStatement -> {
            callableStatement.execute();
            ResultSet Rs = callableStatement.getResultSet();
            while (Rs.next()){
                Map Row = new HashMap();
                Row.put("民宿名称", Rs.getString("民宿名称"));
                Row.put("年度营业额", Rs.getString("年度营业额"));
                ReList.add(Row);
            }
            return ReList;
        });
        return ReList;
    }

    @Override
    public List SerchHotelRoom() {
        List<Map> ReList = new ArrayList<>();
        String str = " { call myporject.SerchHotelRoom()}";
        jdbcTemplate.execute(connection -> {
            CallableStatement cs = connection.prepareCall(str);
            return cs;
        },(CallableStatementCallback<List>) callableStatement -> {
            callableStatement.execute();
            ResultSet Rs = callableStatement.getResultSet();
//                ResultSet Rs = (ResultSet) callableStatement.getObject(1);
            while (Rs.next()) {
                Map Row = new HashMap();
                Row.put("民宿名称", Rs.getString("民宿名称"));
                Row.put("可用房数量", Rs.getString("可用房数量"));
                ReList.add(Row);
            }
            return ReList;
        } );
        return ReList;

    }

    @Override
    public void MuDiscount(String hotel_name,String Leaser_name,String Lease_Type,Double count) {
        List<Map> ReList = new ArrayList<>();
        String str = " { call myporject.Discount(?,?,?,?)}";
        jdbcTemplate.execute(new CallableStatementCreator() {
            List ResList = new ArrayList();
            @Override
            public CallableStatement createCallableStatement(Connection connection) throws SQLException {
                CallableStatement Cs = connection.prepareCall(str);
                Cs.setString(1,hotel_name);
                Cs.setString(2,Leaser_name);
                Cs.setString(3,Lease_Type);
                Cs.setDouble(4,count);
                return Cs;
            }
        }, callableStatement -> {
            callableStatement.execute();
            return null;
        });
    }

    @Override
    public List GetLenter() {
        List<Map> ReList = new ArrayList<>();
        String str = " { call myporject.GetLenter()}";
        jdbcTemplate.execute(connection -> {
            CallableStatement cs = connection.prepareCall(str);
            return cs;
        },(CallableStatementCallback<List>) callableStatement -> {
            callableStatement.execute();
            ResultSet Rs = callableStatement.getResultSet();
//                ResultSet Rs = (ResultSet) callableStatement.getObject(1);
            while (Rs.next()) {
                Map Row = new HashMap();
                Row.put("民宿名称", Rs.getString("民宿名称"));
                Row.put("预约人数", Rs.getString("预约人数"));
                ReList.add(Row);
            }
            return ReList;
        } );
        return ReList;
    }

    @Override
    public List CountByMonthNum() {
        List<Map> ReList = new ArrayList<>();
        String str = " { call myporject.CountByMonthNum()}";
        jdbcTemplate.execute(connection -> {
            CallableStatement cs = connection.prepareCall(str);
            return cs;
        },(CallableStatementCallback<List>) callableStatement -> {
            callableStatement.execute();
            ResultSet Rs = callableStatement.getResultSet();
//                ResultSet Rs = (ResultSet) callableStatement.getObject(1);
            while (Rs.next()) {
                Map Row = new HashMap();
                Row.put("Month", Rs.getString("月份"));
                Row.put("Count", Rs.getString("租房数量"));
                Row.put("HotelName", Rs.getString("民宿名称"));
                ReList.add(Row);
            }
            return ReList;
        } );
        return ReList;
    }

    @Override
    public List GetActiveTimeByName(String hotel_name) {
        List<Map> ReList = new ArrayList<>();
        String str = " { call myporject.GetActiveTimeByName(?)}";
        jdbcTemplate.execute(new CallableStatementCreator() {
            List ResList = new ArrayList();
            @Override
            public CallableStatement createCallableStatement(Connection connection) throws SQLException {
                CallableStatement Cs = connection.prepareCall(str);
                Cs.setString(1,hotel_name);
                return Cs;
            }
        }, callableStatement -> {
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while (resultSet.next()){
                Map Row = new HashMap();
                Row.put("活动方", resultSet.getString("活动方"));
                Row.put("开始时间", resultSet.getString("开始时间"));
                Row.put("结束时间", resultSet.getString("结束时间"));
                ReList.add(Row);
            }
            return ReList;
        });
        return ReList;
    }

    @Override
    public List GetRentByNameAndMonth(String hotel_name, int Month) {
        List<Map> ReList = new ArrayList<>();
        String str = " { call myporject.GetRentByNameAndMonth(?,?)}";
        jdbcTemplate.execute(new CallableStatementCreator() {
            List ResList = new ArrayList();
            @Override
            public CallableStatement createCallableStatement(Connection connection) throws SQLException {
                CallableStatement Cs = connection.prepareCall(str);
                Cs.setString(1,hotel_name);
                Cs.setInt(2,Month);
                return Cs;
            }
        }, callableStatement -> {
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            while (resultSet.next()){
                Map Row = new HashMap();
                Row.put("民宿名称", resultSet.getString("民宿名称"));
                Row.put("预约数", resultSet.getString("预约数"));

                ReList.add(Row);
            }
            return ReList;
        });
        return ReList;

    }

    @Override
    public void AddRentSericve(Rent R) {

        String str = " { call myporject.GetRentByNameAndMonth(?,?)}";
        jdbcTemplate.execute(new CallableStatementCreator() {
            List ResList = new ArrayList();
            @Override
            public CallableStatement createCallableStatement(Connection connection) throws SQLException {
                CallableStatement Cs = connection.prepareCall(str);
                Cs.setString(1,R.getHotelName());
                Cs.setString(2,R.getRoomNumber());
                Cs.setString(3,R.getName());
                Cs.setInt(4, Integer.parseInt(R.getAge()));
                Cs.setString(5,R.getAge());
                Cs.setString(6,R.getPhone());
                Cs.setString(7, R.getSex());
                Cs.setString(8,R.getIdNumber());
                return Cs;
            }
        }, callableStatement -> {
            callableStatement.execute();

            return null;
        });

    }
}
