package com.YH.service.impl;

import com.YH.entity.Lease;
import com.YH.service.LeaceSerive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class LeaceSeriveImpl implements LeaceSerive {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public List AddLeaser(Lease L) {

        String str ="insert into myporject.lease VALUES (?,?,?,?,?)";
        jdbcTemplate.update(str,L.getReserverName(),L.getHotelName(),L.getRoomNumber(),L.getReseveDays(),L.getResevePrice());
        return null;
    }

    @Override
    public List UpdateLeaser(Lease L) {
        String str="UPDATE myporject.lease SET reserver_name=?,hotel_name=?,room_number=?,reseve_days=?,reseve_price=? WHERE reserver_name=?";
        jdbcTemplate.update(str,L.getReserverName(),L.getHotelName(),L.getRoomNumber(),L.getReseveDays(),L.getResevePrice(),L.getReserverName());
        return null;
    }

    @Override
    public List DeleteLeaser(Lease L) {
        String str="DELETE  FROM myporject.lease WHERE reserver_name=?";
        jdbcTemplate.update(str,L.getReserverName());
        return null;
    }
}
