package com.YH.service;

import com.YH.entity.Active;

public interface ActiveService {
    void AddService(Active A);
    void UpdateService(Active A);
    void DeleteService(Active A);
}
