package com.YH.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (Active)实体类
 *
 * @author makejava
 * @since 2021-12-24 23:16:48
 */
public class Active implements Serializable {
    private static final long serialVersionUID = 346698246763395381L;

    private String activeNumber;

    private String activeName;

    private String activeHotel;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date activeStarttime;
//    @JsonFormat(shape = JsonFormat.Shape.STRING , pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date activeEndtime;

    private String remark;


    public String getActiveNumber() {
        return activeNumber;
    }

    public void setActiveNumber(String activeNumber) {
        this.activeNumber = activeNumber;
    }

    public String getActiveName() {
        return activeName;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    public String getActiveHotel() {
        return activeHotel;
    }

    public void setActiveHotel(String activeHotel) {
        this.activeHotel = activeHotel;
    }

    public Date getActiveStarttime() {


        return activeStarttime;
    }

    public void setActiveStarttime(Date activeStarttime) {
        this.activeStarttime = activeStarttime;
    }

    public Date getActiveEndtime() {

        return activeEndtime;
    }

    public void setActiveEndtime(Date activeEndtime) {


        this.activeEndtime = activeEndtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}

