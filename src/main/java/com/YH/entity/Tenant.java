package com.YH.entity;

import java.io.Serializable;

/**
 * (Tenant)实体类
 *
 * @author makejava
 * @since 2021-12-24 23:16:48
 */
public class Tenant implements Serializable {
    private static final long serialVersionUID = -75442163591557880L;

    private String renterName;

    private String renterType;

    private String renterAge;

    private String renterPhone;

    private String renterSex;

    private Integer cumulativeDays;

    private String idNumber;


    public String getRenterName() {
        return renterName;
    }

    public void setRenterName(String renterName) {
        this.renterName = renterName;
    }

    public String getRenterType() {
        return renterType;
    }

    public void setRenterType(String renterType) {
        this.renterType = renterType;
    }

    public String getRenterAge() {
        return renterAge;
    }

    public void setRenterAge(String renterAge) {
        this.renterAge = renterAge;
    }

    public String getRenterPhone() {
        return renterPhone;
    }

    public void setRenterPhone(String renterPhone) {
        this.renterPhone = renterPhone;
    }

    public String getRenterSex() {
        return renterSex;
    }

    public void setRenterSex(String renterSex) {
        this.renterSex = renterSex;
    }

    public Integer getCumulativeDays() {
        return cumulativeDays;
    }

    public void setCumulativeDays(Integer cumulativeDays) {
        this.cumulativeDays = cumulativeDays;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

}

