package com.YH.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (Room)实体类
 *
 * @author makejava
 * @since 2021-12-24 23:16:48
 */
public class Room implements Serializable {
    private static final long serialVersionUID = 591085251134942263L;

    private String roomNumber;

    private String roomType;
    /**
     * 预约人
     */
    private Object price;
    /**
     * 所属民宿
     */
    private String hotelName;
    /**
     * 入住时间
     */

    private Date inTime;
    /**
     * 预约状态
     */
    private String lentState;


    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public Date getInTime() {
        return inTime;
    }

    public void setInTime(Date inTime) {
        this.inTime = inTime;
    }

    public String getLentState() {
        return lentState;
    }

    public void setLentState(String lentState) {
        this.lentState = lentState;
    }

}

