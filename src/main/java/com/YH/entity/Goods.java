package com.YH.entity;

import java.io.Serializable;

/**
 * (Goods)实体类
 *
 * @author makejava
 * @since 2021-12-24 23:21:16
 */
public class Goods implements Serializable {
    private static final long serialVersionUID = -57119371637273273L;

    private String hotelName;

    private String roomNumber;

    private String goodNumber;

    private String goodName;

    private Integer goodValue;

    private Integer goodCount;


    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getGoodNumber() {
        return goodNumber;
    }

    public void setGoodNumber(String goodNumber) {
        this.goodNumber = goodNumber;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public Integer getGoodValue() {
        return goodValue;
    }

    public void setGoodValue(Integer goodValue) {
        this.goodValue = goodValue;
    }

    public Integer getGoodCount() {
        return goodCount;
    }

    public void setGoodCount(Integer goodCount) {
        this.goodCount = goodCount;
    }

}

