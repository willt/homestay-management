package com.YH.entity;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * (Admin)实体类
 *
 * @author YH
 * @since 2021-12-20 10:59:43
 */
@Component
public class Admin implements Serializable {
    private static final long serialVersionUID = -60600838554455758L;

    private Integer id;

    private String account;

    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

