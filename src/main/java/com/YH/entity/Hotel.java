package com.YH.entity;

import java.io.Serializable;

/**
 * (Hotel)实体类
 *
 * @author makejava
 * @since 2021-12-24 23:16:48
 */
public class Hotel implements Serializable {
    private static final long serialVersionUID = -15830695193598867L;

    private String hotelName;
    /**
     * 负责人
     */
    private String hotelMaster;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 民宿地址
     */
    private String address;
    /**
     * 性别
     */
    private String sex;


    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelMaster() {
        return hotelMaster;
    }

    public void setHotelMaster(String hotelMaster) {
        this.hotelMaster = hotelMaster;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

}

