package com.YH.entity;

import java.io.Serializable;

/**
 * (Lease)实体类
 *
 * @author makejava
 * @since 2021-12-24 23:16:48
 */
public class Lease implements Serializable {
    private static final long serialVersionUID = 277245257559303152L;
    private String reserverName;

    private String hotelName;
    /**
     *
租赁的房间号
     */
    private String roomNumber;
    /**
     * 当前租房的天数
     */
    private Integer reseveDays;
    /**
     * 租金
     */
    private Object resevePrice;


    public String getReserverName() {
        return reserverName;
    }

    public void setReserverName(String reserverName) {
        this.reserverName = reserverName;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getReseveDays() {
        return reseveDays;
    }

    public void setReseveDays(Integer reseveDays) {
        this.reseveDays = reseveDays;
    }

    public Object getResevePrice() {
        return resevePrice;
    }

    public void setResevePrice(Object resevePrice) {
        this.resevePrice = resevePrice;
    }

    @Override
    public String toString() {
        return "Lease{" +
                "reserverName='" + reserverName + '\'' +
                ", hotelName='" + hotelName + '\'' +
                ", roomNumber='" + roomNumber + '\'' +
                ", reseveDays=" + reseveDays +
                ", resevePrice=" + resevePrice +
                '}';
    }
}

