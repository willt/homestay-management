package com.YH.entity;

import com.google.gson.Gson;
import lombok.Data;

import java.util.List;

@Data
public class MyData {
    private String meta="200";
    private List list;
    public MyData(String meta){
        this.meta=meta;
    }
    public MyData(List list){
        this.list=list;
    }

    public String GetDatas() {
        return new Gson().toJson(this);
    }
}
