package com.YH.entity;

import java.io.Serializable;

/**
 * (Rent)实体类
 *
 * @author makejava
 * @since 2021-12-24 23:16:48
 */
public class Rent implements Serializable {
    private static final long serialVersionUID = 463023920124435329L;
    /**
     * 预约人姓名
     */
    private String name;
    /**
     * 预约的房间号
     */
    private String roomNumber;
    /**
     * 预约的天数
     */
    private Integer recerverDays;

    private String hotelName;

    private String age;

    private String phone;

    private String idNumber;

    private String sex;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getRecerverDays() {
        return recerverDays;
    }

    public void setRecerverDays(Integer recerverDays) {
        this.recerverDays = recerverDays;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

}

