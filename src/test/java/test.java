import com.YH.dao.impl.AdminDaoimpl;
import com.YH.entity.MyData;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class test implements Serializable {

    /**
     * code : 200
     * msg : 登陆成功
     * data : {"id":1,"account":"admin","password":"2424736543"}
     */

    private String code;
    private String msg;



    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("SpringContext.xml");
        AdminDaoimpl bean = context.getBean(AdminDaoimpl.class);

        List list = bean.GetUsers();
        MyData data = new MyData(list);
        String s = data.GetDatas();

        System.out.println(s);
    }
//    @Data
//    public static class MyData {
//        private String meta="200";
//        private List data;
//    }


}

