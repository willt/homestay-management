package com.YH.dao.impl;

import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LeaseimplTest {

    @Test
    void getAllLeases() {
        ApplicationContext context = new ClassPathXmlApplicationContext("SpringContext.xml");
        Leaseimpl bean = context.getBean(Leaseimpl.class);
        List list = bean.GetAllLeases();
        list.forEach(i->{
            System.out.println(i);
        });

    }
}
