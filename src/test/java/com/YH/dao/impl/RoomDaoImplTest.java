package com.YH.dao.impl;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RoomDaoImplTest {

    @Test
    void getAllRoom() {
        ApplicationContext context = new ClassPathXmlApplicationContext("SpringContext.xml");
        RoomDaoImpl bean = context.getBean(RoomDaoImpl.class);
        List list = bean.GetAllRoom();
        list.forEach(i->{
            System.out.println(i);
        });
        System.out.println(new Gson().toJson(list));
    }
}
