package com.YH.service.impl;

import com.YH.entity.Active;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

class ActiveServiceImplTest {

    @Test
    void updateService() {
        ApplicationContext context = new ClassPathXmlApplicationContext("SpringContext.xml");
        ActiveServiceImpl bean = context.getBean(ActiveServiceImpl.class);
        Active A = new Active();
        A.setActiveNumber("1");
        A.setActiveHotel("三方旅馆");
        A.setActiveName("药狗");
        /*
        active_number
active_name
active_hotel
active_starttime
active_endtime
Remark
         */
        bean.UpdateService(A);
    }

    @SneakyThrows
    @Test
    void addService() {

        ApplicationContext context = new ClassPathXmlApplicationContext("SpringContext.xml");
        ActiveServiceImpl bean = context.getBean(ActiveServiceImpl.class);
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        ft.setLenient(false);
        Active A = new Active();
        A.setActiveNumber("5");
        A.setActiveHotel("三方旅馆");
        A.setActiveName("药狗");
        A.setActiveStarttime(ft.parse("2027-10-12"));
        System.out.println(A.getActiveStarttime());
        bean.AddService(A);
    }


    @Test
    void deleteService() {
        ApplicationContext context = new ClassPathXmlApplicationContext("SpringContext.xml");
        ActiveServiceImpl bean = context.getBean(ActiveServiceImpl.class);
        Active A = new Active();
        A.setActiveNumber("2");
        A.setActiveHotel("三方旅馆");
        A.setActiveName("药狗");
        bean.DeleteService(A);
    }
}
